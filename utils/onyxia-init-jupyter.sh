#!/bin/sh

TARGET=$1
LANGUAGE=$2

WORK_DIR=/home/jovyan/work
CLONE_DIR=exercices-snds

IPYNB_PATH=notebooks/Notebook_${TARGET}_${LANGUAGE}_Lab.ipynb

# Clone course repository
REPO_URL=https://gitlab.com/healthdatahub/formation/exercices-snds.git
git clone --depth 1 $REPO_URL ${WORK_DIR}/$CLONE_DIR

# Install dependencies if needed

# Open the relevant notebook when starting Jupyter Lab
echo "c.LabApp.default_url = '/lab/tree/${CLONE_DIR}/${IPYNB_PATH}'" >> /home/jovyan/.jupyter/jupyter_server_config.py
